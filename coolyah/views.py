from django.shortcuts import render , get_object_or_404, redirect
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import FormMatkul

def tambah_matkul_view(request) :

    form = FormMatkul()

    if request.method == "POST":
        form = FormMatkul(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    data = MataKuliah.objects.all()

    response = {
        "form" : form,
        "listMatkul" : data,
    }

    return render(request, "mataKuliah/form-matkul.html", response)


def detail_matkul_view(request, id):

    obj = get_object_or_404(MataKuliah, id=id)
    response = {
        "listMatkul" : obj,
    }
    return render(request, "mataKuliah/mata-kuliah.html", response)

def delete_view(request, id):

    obj = get_object_or_404(MataKuliah, id=id)
    response = {
        "matkul" : obj,
    }

    if request.method == "POST" :
        if request.POST['conf'] == 'Yes':
            obj.delete()
            return redirect("/") 
        else:
            return redirect("/")
    
    return render(request, "mataKuliah/confirm-hapus.html", response)
from django import forms
from .models import MataKuliah

class FormMatkul(forms.ModelForm) :
    class Meta:
        model = MataKuliah
        fields = [
            'nama',
            'dosen',
            'sks',
            'deskripsi',
            'tahun',
            'ruangan',
        ]

        widgets = {
            'nama' : forms.TextInput(attrs={'class' : 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class' : 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class' : 'form-control'}),
            'tahun' : forms.TextInput(attrs={'class' : 'form-control'}),
            'ruangan' : forms.TextInput(attrs={'class' : 'form-control'}),
            'deskripsi' : forms.Textarea(attrs={'class' : 'form-control', 'rows':4}),
        }

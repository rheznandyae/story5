from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

class MataKuliah(models.Model) :
    nama = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    sks = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(6)])
    deskripsi = models.TextField(blank=True , null=True)
    tahun = models.CharField(max_length = 20)
    ruangan = models.CharField(max_length = 50)

    

